# Tests unitaires | Projet Final | BACK
##### Auteurs : [Enzo Avagliano](https://github.com/eloxfire) & [Alexandre Brun Giglio](https://github.com/AlexBrunGiglio)


## Getting started
- Clonez le projet avec `git clone https://gitlab.com/ynov-cours/m1-test-unitaires/m1-tests-unitaires-api.git`
- Installez les dépendences avec `npm i`
- Installez la base de données en exécutant dans votre client MySQL le script nommé `pre-start-script.sql` mis à disposition à la racine du projet
- Lancez le serveur avec `npm start`
- Rendez vous sur `http://localhost:8000/products/populate` pour remplir la base de données.

Voilà, votre projet est configuré

## Running the tests
- Utilisez `npm run test` pour effectuer les tests du projet

## Available routes :
La base de toutes les url est la suivante : `http://localhost:8000`.

- /
  - Retourne 'OK' (Permet de vérifier que le serveur est bien lancé)
- /products
  - /getAll
    - Retourne un tableau de tous les produits disponible
  - /get/:id
    - Retourne un produit choisi grace à son ID
- /cart
  - /get
    - /subtotal
      - Retourne le montant total du panier
  - /add
    - Ajoute un produit au panier
  - /delete/:id
    - Suprime le produit spécifié du panier

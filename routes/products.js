const express = require('express');
const router = express.Router();
const db = require('../src/databaseModule.js');
const { multer, storage } = require('../src/multerModule');
const axios = require('axios');
const authModule = require('../src/authModule.js');

router.get('/', function (req, res, next) {
  res.send("OK");
});

router.get('/getAll', (req, res) => {
  db.query("SELECT * FROM products", function (err, rows, fields) {
    if (err) {
      res.status(500).end();
      console.log(err);
    } else {
      console.log(rows);
      res.send(rows);
    }
  });
});

router.get('/get/:id', (req, res) => {
  db.query(`SELECT * FROM products WHERE id = ${req.params.id}`, function (err, rows, fields) {
    if (err) {
      res.status(500).end();
      console.log(err);
    } else {
      console.log(rows);
      res.send(rows);
    }
  });
});

router.get('/populate', function (req, res, next) {

  const getRndInteger = (min, max) => {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  axios.get('https://rickandmortyapi.com/api/character/', { params: { page: 10 } })
    .then((response) => {

      response.data.results.map((item) => {
        let values = [
          [item.name, item.status, item.species, item.type, item.gender, item.image, getRndInteger(10, 120)]
        ]
        let sql = 'INSERT INTO products (name, status, species, type, gender, image, price) VALUES ?';
        db.query(sql, [values], function (err, result) {
          if (err) {
            console.log(err);
          }
        });
      })

      res.send(response.data.results);

    });
});

module.exports = router;

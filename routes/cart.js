const express = require('express');
const router = express.Router();
const db = require('../src/databaseModule.js');
const authModule = require('../src/authModule.js');

router.get('/', function (req, res, next) {
  res.send("OK");
});

router.get('/get', function (req, res, next) {
  db.query("SELECT * FROM cart", function (err, rows, fields) {
    if (err) {
      res.status(500).end();
      console.log(err);
    } else {
      res.status(200).send(rows);
    }
  });
});

router.get('/get/subtotal', function (req, res, next) {
  db.query("SELECT product_price_unit, product_quantity FROM cart", function (err, rows, fields) {
    if (err) {
      res.status(500).end();
      console.log(err);
    } else {
      let rowsTotalPrices = [];
      for (let i = 0; i < rows.length; i++) {
        const rowTotal = rows[i].product_price_unit * rows[i].product_quantity;
        rowsTotalPrices.push(rowTotal);
      }
      const sum = rowsTotalPrices.reduce((partialSum, a) => partialSum + a, 0);
      // console.log(rows);
      res.status(200).send(`${sum}`);
    }
  });
});

router.post('/add', function (req, res) {
  const body = req.body;

  db.query(`INSERT INTO cart(product_id, product_name, product_quantity, product_price_unit, product_price_total, product_image) VALUES ('${body.id}', "${body.name}", ${body.quantity}, ${body.price}, ${body.price * body.quantity}, '${body.image}')`, function (err, rows, fields) {
    if (err) {
      res.status(500).end();
      console.log(err);
    } else {
      res.status(200).send(rows);
    }
  });
});

router.post('/delete/:id', function (req, res) {

  db.query(`DELETE FROM cart WHERE product_id = ${req.params.id}`, function (err, result) {
    if (err) {
      res.status(500).end();
      console.log(err);
    } else {
      res.status(200).send({ response: "ok" });
    }
  })
})
module.exports = router;

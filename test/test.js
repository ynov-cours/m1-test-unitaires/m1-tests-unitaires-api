const request = require('supertest');
const app = require('../app');

describe('=== App status test ===', function () {
  it('Has a default page', function (done) {
    request(app)
      .get('/')
      .expect(200)
      .expect('OK', done);
  });
});

describe('=== GET Products test ===', function () {
  it('Get all products', function (done) {
    request(app)
      .get('/products/getAll')
      .expect(200)
      .expect('Content-Type', /json/, done);
  });
});

describe('=== GET Specific product test ===', function () {
  it('Get specific product by id', function (done) {
    request(app)
      .get('/products/get/1')
      .expect(200)
      .expect('Content-Type', /json/, done);
  });
});

describe('=== GET Cart sum test ===', function () {
  it('Get cart subtotal', function (done) {
    request(app)
      .get('/cart/get/subtotal')
      .expect(200)
      .expect('Content-Type', /text/, done);
  });
});
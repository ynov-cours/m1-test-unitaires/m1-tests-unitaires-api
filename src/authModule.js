const config = require('../config.js');

function authorize(request, response){
  let token;
  if(request.query){
    token = request.query.token;
  }else{
    let token = "notoken";
  }

  if(token != config.AUTH.TOKEN){
    response.status(401).end();
    return false;
  }else{
    return true;
  }
}

module.exports = authorize;

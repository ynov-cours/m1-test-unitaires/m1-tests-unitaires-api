const mysql = require('mysql');
const settings = require('../config.js');
const boxen = require('boxen');
let db;

function connectDatabase() {
  if (!db) {
    db = mysql.createConnection({
      host: settings.MYSQL.HOST,
      port: settings.MYSQL.PORT,
      user: settings.MYSQL.USER,
      password: settings.MYSQL.PASS,
      database: settings.MYSQL.DATABASE
    });

    db.connect(function(err){
      if(!err) {
        console.log('\x1b[0m\x1b[37m => Database : \x1b[1m\x1b[4m\x1b[32mConnected\n\x1b[0m');
      } else {
        console.log('\x1b[0m\x1b[37m => Database : \x1b[1m\x1b[4m\x1b[31mError\n\x1b[0m');
        console.log(err);
      }
    });
  }
  return db;
}

module.exports = connectDatabase();

// Copiez et renomez ce fichier config.js

module.exports = {
  MYSQL: {
    HOST: "host",
    PORT: 3306,
    USER: "db_user",
    PASS: "db_user_pass",
    DATABASE: "avagliano_brun_giglio_test_unitaires_final_project",
  }
}
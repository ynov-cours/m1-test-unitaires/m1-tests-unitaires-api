DROP DATABASE IF EXISTS avagliano_test_unitaires_final_project;
CREATE DATABASE IF NOT EXISTS avagliano_test_unitaires_final_project;
USE avagliano_test_unitaires_final_project;

CREATE TABLE `products` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) DEFAULT '',
	`status` VARCHAR(255) DEFAULT '',
	`species` VARCHAR(255) DEFAULT '',
	`type` VARCHAR(255) DEFAULT '',
	`gender` VARCHAR(255) DEFAULT '',
	`image` TEXT(1000),
	`price` INT,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;


CREATE TABLE `cart` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`product_id` INT NOT NULL,
	`product_name` VARCHAR(255) DEFAULT '',
	`product_quantity` INT,
	`product_price_unit` INT,
	`product_price_total` INT,
	`product_image` TEXT,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;